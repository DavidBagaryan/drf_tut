from django.contrib.auth.models import User, Group
from rest_framework import viewsets

from .serializers import UserSerializer, GroupSerializer


class UserViesSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViesSet(viewsets.ModelViewSet):
    queryset = Group.objects.all().order_by('-date_joined')
    serializer_class = GroupSerializer
